package PredictBreastCancerSubtype;

import java.io.IOException;

public class BreastCancerPredictorApp {

    public static void main(String[] args) throws IOException {
        ApacheCliOptionsProvider cliOptions = new ApacheCliOptionsProvider(args);

        if (cliOptions.helpRequested()) {
            cliOptions.printHelp();
            return;
        }

        if (cliOptions.attributeOrderRequested()) {
//            System.out.println("TEST");
            cliOptions.printAttributeOrder();
            return;
        }

        if (cliOptions.datafileReceived()) {
            String unknowndatafile = cliOptions.getUnknownDatafile();
            WekaRunner wekaRunner = new WekaRunner(unknowndatafile);
            wekaRunner.start();
        }

        if (cliOptions.instanceReceived()) {
            String userInputFile = "data/userinputfile.arff";
            String instance = cliOptions.getUnknownInstance();
            InstanceToFileWriter instanceToFileWriter = new InstanceToFileWriter();
            instanceToFileWriter.start(instance);
            WekaRunner wekaRunner = new WekaRunner(userInputFile);
            wekaRunner.start();
        }
    }
}
