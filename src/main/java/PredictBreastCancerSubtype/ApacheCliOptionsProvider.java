package PredictBreastCancerSubtype;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String ORDER = "order";
    private static final String DATAFILE = "datafile";
    private static final String INSTANCE = "instance";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String datafile;
    private String instance;

    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    public boolean attributeOrderRequested() { return this.commandLine.hasOption(ORDER); }

    public boolean instanceReceived() {
        return this.commandLine.hasOption(INSTANCE);
    }

    public boolean datafileReceived() {
        return this.commandLine.hasOption(DATAFILE);
    }

    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option instanceAttrOption = new Option("o", ORDER, false, "Gives the order the attributes need to be in when giving an instance as input");
        Option datafileOption = new Option("n", DATAFILE, true, "Datafile to be classified");
        Option instanceOption = new Option("i", INSTANCE, true, "Instance to be classified. Needs to be 30 values");

        options.addOption(helpOption);
        options.addOption(instanceAttrOption);
        options.addOption(datafileOption);
        options.addOption(instanceOption);
    }

    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
//            for (String arg : this.commandLine.getArgs()) {
//                System.out.println(arg);
//            }
            if (commandLine.hasOption(DATAFILE)) {
                String datafile = commandLine.getOptionValue(DATAFILE);
                if (isLegalDatafileType(datafile)) {
                    this.datafile = datafile;
                } else {
                    throw new IllegalArgumentException("Datafile not valid. Datafile given:\"" + datafile + "\"");
                }
            } else if (commandLine.hasOption(INSTANCE)) {
                String instance = commandLine.getOptionValue(INSTANCE);
                if (isLegalDataInstance(instance)) {
                    this.instance = instance;
                } else {
                    throw new IllegalArgumentException("Instance has to little values. Amount of values needed = 29.");
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private boolean isLegalDatafileType(String datafile) {
        try {
            if (datafile.endsWith(".arff")) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println("Filetype not valid");
        }
        return false;
    }

    private boolean isLegalDataInstance(String i) {
        try {
            String[] instanceValues = commandLine.getOptionValue(INSTANCE).split(",");
            if (instanceValues.length == 29) {
                return true;
            } else {
                throw new IllegalArgumentException("Instance not valid, instance is supposed to have 30 values. " + instanceValues.toString());
            }
        } catch (Exception ex) {
            System.out.println("Instance not valid");
        }
        return false;
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("WekaWrapperThema09Joas", options);
    }

    public void printAttributeOrder() throws IOException {
        try {
            BufferedReader r = new BufferedReader(new FileReader("data/attributeOrder"));
            StringBuilder s = new StringBuilder();
            String line = null;
            while ((line = r.readLine()) != null) {
                s.append(line).append("\n");
            }
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUnknownDatafile() {
        return this.datafile;
    }

    @Override
    public String getUnknownInstance() {
        return this.instance;
    }
}

