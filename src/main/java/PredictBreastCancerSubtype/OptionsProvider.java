package PredictBreastCancerSubtype;

public interface OptionsProvider {
    /**
     * serves the name of the application user.
     * @return userName the user name
     */
    String getUnknownDatafile();

    String getUnknownInstance();

}
