package PredictBreastCancerSubtype;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.IOException;

public class WekaRunner {
    private final static String modelFile = "data/model_breast_cancer_subtypes.model";
    private final String datafile;
    private final String unknownInput;

    public WekaRunner(String unknownInput) {
        this.datafile = "data/clinical_data_breast_cancer_no_dupes_class_at_end.arff";
        this.unknownInput = unknownInput;
    }

//    public static void main(String[] args) {
//        WekaRunner runner = new WekaRunner("data/testdata.arff");
//        runner.start();
//    }

    protected void start() {
        try {
//            Instances instances = loadArff(datafile);
//            printInstances(instances);
            J48 fromFile = loadClassifier();
            Instances unknownInstances = loadArff(unknownInput);
            classifyNewInstances(fromFile, unknownInstances);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource dataSource = new DataSource(datafile);
            Instances data = dataSource.getDataSet();
            data.setClassIndex(data.numAttributes() -1);
            return data;
        } catch (Exception e) {
            throw new IOException("File unreadable");
        }
    }

    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("Attribute" + i + " = " + instances.attribute(i));
        }

        int numInstances = instances.numInstances();

        for (int i = 0; i < numInstances; i++) {
            System.out.println("Instance = " + instances.instance(i));
        }
    }

    private J48 loadClassifier() throws Exception {
        return (J48) weka.core.SerializationHelper.read(modelFile);
    }

    private void classifyNewInstances(J48 tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }
}
