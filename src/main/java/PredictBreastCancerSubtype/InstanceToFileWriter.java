package PredictBreastCancerSubtype;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

public class InstanceToFileWriter {

    public void start(String instance) {
        String attributes = "data/arfftemplate.arff";
        String userInputFile = "data/userinputfile.arff";

        InstanceToFileWriter instanceToFileWriter = new InstanceToFileWriter();
        instanceToFileWriter.copyFile(attributes, userInputFile);
        instanceToFileWriter.writeToFile(userInputFile, instance);
    }

    private void copyFile(String attributes, String userInputFile) {
        Path path = Paths.get(attributes);
        try {
            Path copy = Paths.get(userInputFile);
            Files.copy(path, copy, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String userInputFile, String instance) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(userInputFile),
                StandardOpenOption.APPEND)) {
            writer.write("?," + instance);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}