package FilesNotUsed;

import PredictBreastCancerSubtype.OptionsProvider;

public final class UserInputRequesterRunner {
    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private UserInputRequesterRunner() { }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        OptionsProvider op = new UserInputOptionsProvider();
        MessagingController controller = new MessagingController(op);
        controller.start();
    }
}
