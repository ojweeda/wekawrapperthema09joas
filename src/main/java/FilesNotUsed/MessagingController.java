package FilesNotUsed;

import PredictBreastCancerSubtype.OptionsProvider;

import java.io.File;

public class MessagingController {
    private final OptionsProvider optionsProvider;

    /**
     * the constructor needs an OptionsProvider to be able to do its work.
     * @param optionsProvider the options provider
     */
    public MessagingController(final OptionsProvider optionsProvider) {
        this.optionsProvider = optionsProvider;
    }

    /**
     * Starts the application logic.
     */
    public void start() {
        if (optionsProvider == null) {
            throw new IllegalStateException("No use going on without an optionsController.");
        } else {
            printUserSettings();
        }
        System.out.println("\nDone");
    }

    /**
     * prints all application settings.
     */
    private void printUserSettings() {
        String givenDatafile = optionsProvider.getUnknownDatafile();
        System.out.print("Given datafile: " + givenDatafile);
    }
}
