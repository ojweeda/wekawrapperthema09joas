package FilesNotUsed;

import PredictBreastCancerSubtype.OptionsProvider;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInputOptionsProvider implements OptionsProvider {
    private String unknownDatafile;
    private String unknownInstance;

    public UserInputOptionsProvider() {
        initialize();
    }

    private void initialize() {
        Scanner scanner = new Scanner(System.in);
        fetchUnknownDatafile(scanner);
    }

    private void fetchUnknownDatafile(final Scanner scanner) {
        String uDatafile = "";
        System.out.println("Please enter the name of the arff datafile you want to classify: ");
        try {
            uDatafile = scanner.next();
        } catch (InputMismatchException ex) {
            System.err.println("Error, input is non-valid");
            System.exit(0);
        }
        this.unknownDatafile = uDatafile;
    }

    @Override
    public String getUnknownDatafile() {
        return unknownDatafile;
    }

    @Override
    public String getUnknownInstance() {
        return unknownInstance;
    }
}
