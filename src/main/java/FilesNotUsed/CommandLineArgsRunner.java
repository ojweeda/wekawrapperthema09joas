package FilesNotUsed;

import PredictBreastCancerSubtype.ApacheCliOptionsProvider;

import java.util.Arrays;

public final class CommandLineArgsRunner {

    private CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }
            MessagingController controller = new MessagingController(op);
            controller.start();
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
