# README #
Author: Joas Weeda
Date: 16/11/2020

### What is this repository for? ###

This README shows you how to run the program and the different funcionalities of it.

### How do I get set up? ###

1. Clone this repo
2. Use a terminal to go to the repo folder
3. Make sure your current directory ends with: "wekawrapperthema09joas"
4. There are 4 different options to run the program from here
    1. start with: java -jar build\libs\wekawrapperthema09joas-1.0-SNAPSHOT-all.jar
    2. For the help option: add -h
    3. For the order the attributes need to be in when giving a single instance: add -o
    4. When you want to input an arff datafile: -n data/testdata.arff
    5. When you want to input a single instance: -i "FEMALE,53,Negative,Negative,Positive,T2,T_Other,
    N2,Positive,M0,Negative,'Stage IIIA',No_Conversion,followup,LIVING,1965,?,0,1965,-5,-11,4,4,Her2,5,1,1,1,?"
    make sure you input 29 values. Look at -o for the attributes and the choices you have per attribute

